import { deleteMoney, noMoney, getRandom, addMoney } from '../files/functions.js'

if (sessionStorage.getItem('money')) {
	if (document.querySelector('.score')) {
		document.querySelectorAll('.score').forEach(el => {
			el.textContent = sessionStorage.getItem('money');
		})
	}
} else {
	sessionStorage.setItem('money', 12000);
	if (document.querySelector('.score')) {
		document.querySelectorAll('.score').forEach(el => {
			el.textContent = sessionStorage.getItem('money');
		})
	}
}


//========================================================================================================================================================
// Функция присвоения случайного класса анимациии money icon
const anim_items = document.querySelectorAll('.icon-anim img');
function getRandomAnimate() {
	let number = getRandom(0, 3);
	let arr = ['jump', 'scale', 'rotate'];
	let random_item = getRandom(0, anim_items.length);
	anim_items.forEach(el => {
		if (el.classList.contains('_anim-icon-jump')) {
			el.classList.remove('_anim-icon-jump');
		} else if (el.classList.contains('_anim-icon-scale')) {
			el.classList.remove('_anim-icon-scale');
		} else if (el.classList.contains('_anim-icon-rotate')) {
			el.classList.remove('_anim-icon-rotate');
		}
	})
	setTimeout(() => {
		anim_items[random_item].classList.add(`_anim-icon-${arr[number]}`);
	}, 100);
}

if (document.querySelector('.icon-anim img')) {
	setInterval(() => {
		getRandomAnimate();
	}, 20000);
}

const btnHome = document.querySelector('.controls-slot__btn-home');
function getRandomAnimate2() {
	let number = getRandom(1, 3);

	if (btnHome.classList.contains('_anim-1')) {
		btnHome.classList.remove('_anim-1');
	} else if (btnHome.classList.contains('_anim-2')) {
		btnHome.classList.remove('_anim-2');
	}

	setTimeout(() => {
		btnHome.classList.add(`_anim-${number}`);
	}, 100);
}

if (btnHome) {
	setInterval(() => {
		getRandomAnimate2();
	}, 20000);
}

// Функция-предохранитель - если в банке будет очень много денег, уменьшает шрифт текста
function checkLengthScore() {
	const score = document.querySelector('.score-box__score');
	let length = score.clientWidth;
	if (length > 120) {
		if (score.classList.contains('_txt-middle')) score.classList.remove('_txt-middle');
		score.classList.add('_txt-small');
	}
	else if (length > 100 && length < 120) {
		if (score.classList.contains('_txt-small')) score.classList.remove('_txt-small');
		score.classList.add('_txt-middle');
	}
	else {
		if (score.classList.contains('_txt-middle')) score.classList.remove('_txt-middle');
		if (score.classList.contains('_txt-small')) score.classList.remove('_txt-small');
	}
}

//========================================================================================================================================================
if (document.querySelector('.wrapper')) checkLengthScore();

//========================================================================================================================================================
const configSlot = {
	currentWin: 0,
	winCoeff: 20,
	balance: 0,
	bet: 100
}

//game-1
if (document.querySelector('.slot__body')) {
	document.querySelector('.slot__body').classList.add('_active');
	sessionStorage.setItem('current-bet', 100);
	document.querySelector('.score').textContent = sessionStorage.getItem('money');
}
let slot1 = null;
let slot2 = null;

//========================================================================================================================================================
class Slot1 {
	constructor(domElement, config = {}) {
		Symbol1.preload();

		this.currentSymbols = [
			["1", "2", "3"],
			["2", "3", "1"],
			["3", "1", "2"],
		];

		this.nextSymbols = [
			["1", "2", "3"],
			["2", "3", "1"],
			["3", "1", "2"],
		];

		this.container = domElement;

		this.reels = Array.from(this.container.getElementsByClassName("reel1")).map(
			(reelContainer, idx) =>
				new Reel1(reelContainer, idx, this.currentSymbols[idx])
		);

		this.spinButton = document.querySelector('.controls-slot__button-spin');
		this.spinButton.addEventListener("click", () => {
			if ((+sessionStorage.getItem('money') >= +sessionStorage.getItem('current-bet'))) {
				this.spin();
			} else {
				noMoney('.score');
			}
		});


		if (config.inverted) {
			this.container.classList.add("inverted");
		}
		this.config = config;
	}

	spin() {
		this.currentSymbols = this.nextSymbols;
		this.nextSymbols = [
			[Symbol1.random(), Symbol1.random(), Symbol1.random()],
			[Symbol1.random(), Symbol1.random(), Symbol1.random()],
			[Symbol1.random(), Symbol1.random(), Symbol1.random()]
		];

		this.onSpinStart(this.nextSymbols);

		return Promise.all(
			this.reels.map((reel) => {
				reel.renderSymbols(this.nextSymbols[reel.idx]);
				return reel.spin();
			})
		).then(() => this.onSpinEnd(this.nextSymbols));
	}

	onSpinStart(symbols) {
		deleteMoney(configSlot.bet, '.score', 'money');

		this.spinButton.classList.add('_hold');

		this.config.onSpinStart?.(symbols);
	}

	onSpinEnd(symbols) {
		this.spinButton.classList.remove('_hold');

		this.config.onSpinEnd?.(symbols);
	}
}

class Reel1 {
	constructor(reelContainer, idx, initialSymbols) {
		this.reelContainer = reelContainer;
		this.idx = idx;

		this.symbolContainer = document.createElement("div");
		this.symbolContainer.classList.add("icons");
		this.reelContainer.appendChild(this.symbolContainer);

		this.animation = this.symbolContainer.animate(
			[
				{ transform: "none", filter: "blur(0)" },
				{ filter: "blur(2px)", offset: 0.5 },
				{
					transform: `translateY(-${((Math.floor(this.factor) * 10) /
						(3 + Math.floor(this.factor) * 10)) *
						100
						}%)`,
					filter: "blur(0)",
				},
			],
			{
				duration: this.factor * 1000,
				easing: "ease-in-out",
			}
		);
		this.animation.cancel();
		initialSymbols.forEach((symbol) =>
			this.symbolContainer.appendChild(new Symbol1(symbol).img)
		);
	}

	get factor() {
		return 3 + Math.pow(this.idx / 2, 2);
	}

	renderSymbols(nextSymbols) {
		const fragment = document.createDocumentFragment();

		for (let i = 3; i < 3 + Math.floor(this.factor) * 10; i++) {
			const icon = new Symbol1(
				i >= 10 * Math.floor(this.factor) - 2
					? nextSymbols[i - Math.floor(this.factor) * 10]
					: undefined
			);
			fragment.appendChild(icon.img);
		}

		this.symbolContainer.appendChild(fragment);
	}

	spin() {
		const animationPromise = new Promise(
			(resolve) => (this.animation.onfinish = resolve)
		);
		const timeoutPromise = new Promise((resolve) =>
			setTimeout(resolve, this.factor * 1000)
		);

		this.animation.play();

		return Promise.race([animationPromise, timeoutPromise]).then(() => {
			if (this.animation.playState !== "finished") this.animation.finish();

			const max = this.symbolContainer.children.length - 3; // 3 - количество картинок в одной колонке после остановки

			for (let i = 0; i < max; i++) {
				this.symbolContainer.firstChild.remove();
			}
		});
	}
}

const cache1 = {};

class Symbol1 {
	constructor(name = Symbol1.random()) {
		this.name = name;

		if (cache1[name]) {
			this.img = cache1[name].cloneNode();
		} else {

			this.img = new Image();
			this.img.src = `img/game-1/slot-${name}.png`;

			cache1[name] = this.img;
		}
	}

	static preload() {
		Symbol1.symbols.forEach((symbol) => new Symbol1(symbol));
	}

	static get symbols() {
		return [
			'1',
			'2',
			'3'
		];
	}

	static random() {
		return this.symbols[Math.floor(Math.random() * this.symbols.length)];
	}
}

const config1 = {
	inverted: false,
	onSpinStart: (symbols) => {
	},
	onSpinEnd: (symbols) => {
		if (symbols[0][1] == symbols[1][1] && symbols[1][1] == symbols[2][1]) {

			let currintWin = configSlot.bet * configSlot.winCoeff;

			// Записываем сколько выиграно на данный момент
			configSlot.currentWin += currintWin;
			addMoney(currintWin, '.score', 1000, 2000);
			checkLengthScore();
		}
	},
};

if (document.querySelector('.wrapper_game-1')) {

	slot1 = new Slot1(document.getElementById("slot1"), config1);
}

//========================================================================================================================================================



class Slot2 {
	constructor(domElement, config = {}) {
		Symbol2.preload();

		this.currentSymbols = [
			["1", "2", "3"],
			["2", "3", "1"],
			["3", "1", "2"],
		];

		this.nextSymbols = [
			["1", "2", "3"],
			["2", "3", "1"],
			["3", "1", "2"],
		];

		this.container = domElement;

		this.reels = Array.from(this.container.getElementsByClassName("reel2")).map(
			(reelContainer, idx) =>
				new Reel2(reelContainer, idx, this.currentSymbols[idx])
		);

		this.spinButton = document.querySelector('.controls-slot__button-spin');
		this.spinButton.addEventListener("click", () => {
			if ((+sessionStorage.getItem('money') >= +sessionStorage.getItem('current-bet'))) {
				this.spin();
			} else {
				noMoney('.score');
			}
		});


		if (config.inverted) {
			this.container.classList.add("inverted");
		}
		this.config = config;
	}

	spin() {
		this.currentSymbols = this.nextSymbols;
		this.nextSymbols = [
			[Symbol2.random(), Symbol2.random(), Symbol2.random()],
			[Symbol2.random(), Symbol2.random(), Symbol2.random()],
			[Symbol2.random(), Symbol2.random(), Symbol2.random()]
		];

		this.onSpinStart(this.nextSymbols);

		return Promise.all(
			this.reels.map((reel) => {
				reel.renderSymbols(this.nextSymbols[reel.idx]);
				return reel.spin();
			})
		).then(() => this.onSpinEnd(this.nextSymbols));
	}

	onSpinStart(symbols) {
		deleteMoney(configSlot.bet, '.score', 'money');

		this.spinButton.classList.add('_hold');

		this.config.onSpinStart?.(symbols);
	}

	onSpinEnd(symbols) {
		this.spinButton.classList.remove('_hold');

		this.config.onSpinEnd?.(symbols);
	}
}

class Reel2 {
	constructor(reelContainer, idx, initialSymbols) {
		this.reelContainer = reelContainer;
		this.idx = idx;

		this.symbolContainer = document.createElement("div");
		this.symbolContainer.classList.add("icons");
		this.reelContainer.appendChild(this.symbolContainer);

		this.animation = this.symbolContainer.animate(
			[
				{ transform: "none", filter: "blur(0)" },
				{ filter: "blur(2px)", offset: 0.5 },
				{
					transform: `translateY(-${((Math.floor(this.factor) * 10) /
						(3 + Math.floor(this.factor) * 10)) *
						100
						}%)`,
					filter: "blur(0)",
				},
			],
			{
				duration: this.factor * 1000,
				easing: "ease-in-out",
			}
		);
		this.animation.cancel();

		initialSymbols.forEach((symbol) =>
			this.symbolContainer.appendChild(new Symbol2(symbol).img)
		);
	}

	get factor() {
		return 1 + Math.pow(this.idx / 2, 2);
	}

	renderSymbols(nextSymbols) {
		const fragment = document.createDocumentFragment();

		for (let i = 3; i < 3 + Math.floor(this.factor) * 10; i++) {
			const icon = new Symbol2(
				i >= 10 * Math.floor(this.factor) - 2
					? nextSymbols[i - Math.floor(this.factor) * 10]
					: undefined
			);
			fragment.appendChild(icon.img);
		}

		this.symbolContainer.appendChild(fragment);
	}

	spin() {
		const animationPromise = new Promise(
			(resolve) => (this.animation.onfinish = resolve)
		);
		const timeoutPromise = new Promise((resolve) =>
			setTimeout(resolve, this.factor * 1000)
		);

		this.animation.play();

		return Promise.race([animationPromise, timeoutPromise]).then(() => {
			if (this.animation.playState !== "finished") this.animation.finish();

			const max = this.symbolContainer.children.length - 3; // 3 - количество картинок в одной колонке после остановки

			for (let i = 0; i < max; i++) {
				this.symbolContainer.firstChild.remove();
			}
		});
	}
}

const cache2 = {};

class Symbol2 {
	constructor(name = Symbol2.random()) {
		this.name = name;

		if (cache2[name]) {
			this.img = cache2[name].cloneNode();
		} else {

			this.img = new Image();
			this.img.src = `img/game-2/slot-${name}.png`;

			cache2[name] = this.img;
		}
	}

	static preload() {
		Symbol2.symbols.forEach((symbol) => new Symbol2(symbol));
	}

	static get symbols() {
		return [
			'1',
			'2',
			'3'
		];
	}

	static random() {
		return this.symbols[Math.floor(Math.random() * this.symbols.length)];
	}
}

const config2 = {
	inverted: false,
	onSpinStart: (symbols) => {
	},
	onSpinEnd: (symbols) => {
		if (symbols[0][0] == symbols[1][0] && symbols[1][0] == symbols[2][0] ||
			symbols[0][1] == symbols[1][1] && symbols[1][1] == symbols[2][1] ||
			symbols[0][2] == symbols[1][2] && symbols[1][2] == symbols[2][2]) {

			let currintWin = +sessionStorage.getItem('current-bet') * configSlot.winCoeff;

			// Записываем сколько выиграно на данный момент
			configSlot.currentWin += currintWin;
			addMoney(currintWin, '.score', 1000, 2000);
			checkLengthScore();
		}
	},
};

if (document.querySelector('.wrapper_game-2')) {
	slot2 = new Slot2(document.getElementById("slot2"), config2);
}



//========================================================================================================================================================
//game-drum

const config_game = {
	last_rotate: 0,
	count_win: 0,
}
export function rotateDrum() {
	config_game.last_rotate += getRandom(100, 2000);
	document.querySelector('.circle__drum').style.transform = `rotate(${config_game.last_rotate}deg)`;
}
export function getTargetBlock() {
	let arrow_top = document.querySelector('.circle__dot').getBoundingClientRect().top - 2.5;
	let arrow_left = document.querySelector('.circle__dot').getBoundingClientRect().left - 2.5;

	let dot = document.createElement('div');
	dot.style.width = `5px`;
	dot.style.height = `5px`;
	dot.style.position = `fixed`;
	dot.style.zIndex = `10`;
	dot.style.top = `${arrow_top}px`;
	dot.style.left = `${arrow_left}px`;

	document.querySelector('.wrapper').append(dot);

	let arrow_top2 = dot.getBoundingClientRect().top + 10;
	let arrow_left2 = dot.getBoundingClientRect().left;
	let target_block2 = document.elementFromPoint(arrow_left2, arrow_top2);

	setTimeout(() => {
		dot.remove();
	}, 1000);
	return target_block2;
}

export function checkTargetItem(block) {
	let value = +block.dataset.target;
	if (value > 0) {
		// пишем логику добавления выигрыша в банк
		addMoney(value, '.score', 1000, 2000);
		checkLengthScore();
	}
}

window.addEventListener('click', (e) => {
	let targetElement = e.target;
	if (targetElement.closest('.header__btn-home')) {
		slot1 = null;
		slot2 = null;
	}
})


